# Question-bank-fe (QBG)
QBG is a comprehensive system containing a diverse array of structured questions and corresponding answers spanning various topics catered to specific classes or batches. This extensive collection enables the automated generation of template-based SEO landing pages through a database-driven approach. Leveraging SEO as a vital user acquisition channel, the integration of these database-driven SEO landing pages is poised to significantly enhance the website's SEO traffic. Additionally, by implementing proper schema markup, the included Q&As can appear as a prominent "Questions and Answers" featured snippet on the Search Engine Results Page (SERP).
​
## Table of Contents
1. [Project Overview](#project-overview)
2. [Getting Started](#getting-started)
3. [Tech Stack](#tech-stack)
4. [Prerequisites](#prerequisites)
5. [Usage](#usage)
6. [Testing](#testing)
7. [Deployments](#deployments)
8. [Contributors](#contributors)
9. [License](#license)
​

## Project Overview <a name="project-overview"></a>
- **Purpose**:
    - Enhancing the acquisition process through search engine functionality, students should easily find the PW Q&A page via Google searches.
    - To optimize user experience, accessing the Q&A page on the PW website should be effortless when searching for questions directly. This ensures a seamless experience for PW's dedicated audience and encourages greater adoption of the Q&A section.
    - PW should support both image and text searches, allowing students to seamlessly utilize Saarthi's Ask Doubt feature on the website.
    - To empower students with targeted practice, the QBG should facilitate topic-wise problem-solving by offering enhanced filters and sorting options, including difficulty level, popularity, and previous year questions. Moreover, an improved categorization system for the QBG is necessary.
    - Clearly define the next actionable steps for users to achieve their desired outcomes.
    ​
- **Features**: 

    - Students have the option to search for questions without the need to log in.
    - Uploading images of questions is a feature that allows students to receive responses.

- **Architecture**: 

    - [Click here to check the architecture](https://physicswallah001.atlassian.net/wiki/spaces/EN/pages/edit-v2/205357073)
    - SSR or SSG pages will be decided based on stress test on NextJs
    - Pre commit hooks will be incorporated – Husky
    - [![Architecture.png](https://i.postimg.cc/DftM94Nk/Architecture.png)](https://postimg.cc/PNQyZJh2)

## Getting Started <a name="getting-started"></a>

Follow these steps to get the project up and running:
​
### Setup Instructions <a name="setup-instruction"></a>

1. **Clone the Repository:**

    - Open your terminal and execute the following commands:
  
    ```bash
    git clone https://gitlab.com/penpencil-services/frontend/question-bank-fe.git --recursive
    ```
   
    - This will clone the project from the specified branch (`initial-commit`) of the GitLab repository. 

    ```bash
    cd Question-bank-fe
    ```
2. **Install npm:**
    - To install npm, run the following command
    ```bash
    npm install --legacy-peer-deps
    ```
3. **Start Development Servers:**
    - Execute the following command:
    ```bash
    npm run dev 
    ```
    - PORT `8000`

    - URL: `localhost:8000/question-answer/`
    - Sample Question url - `http://localhost:8000/question-answer/cell-lacks-cell-wall-190520`
3. **Making Build and start Server:**
    - Execute the following command:
    ```bash 
    npm run build
    npm run start
    ``` 
    - PORT `3000`
    - URL: `localhost:3000/question-answer/`
    - Sample Question url - `http://localhost:3000/question-answer/cell-lacks-cell-wall-190520`
You are now set up to start working on the QBG. Happy coding!
​​
## Tech Stack <a name="tech-stack"></a>
​
List of all technical stack utilized in the project:
​
- NextJS
- react-image-crop
- Webpack
- Typescript
- Husky
- Using Submodules
​
## Prerequisites <a name="prerequisites"></a>

- Node JS

## Usage <a name="usage"></a>
- This project does not require explicit usage instructions

## Testing <a name="testing"></a>

- **Test Scenarios**: [Click here to visit](https://docs.google.com/spreadsheets/d/1iL6_wff88nxDVOQ-SNVXqkhVCbNrgmfSRFnq2jlRlec/edit#gid=617621762)
- **Test Data**: Any.
- **Test Automation**: Not applicable.
- **Bug Reporting**: [Jira](https://physicswallah001.atlassian.net/issues/?jql=project%20%3D%20%22PUA%22%20and%20type%20[…]nd%20labels%20%3D%20QBG%20ORDER%20BY%20created%20DESC )
- **Testing Environment Setup**: Staging.

## Deployments <a name="deployments"></a>

| Environments   | Environment URLs             | Jenkins Jobs       | Deployment Instructions                                     |
|----------------|------------------------------|--------------------|-------------------------------------------------------------|
| Development    | No Dev url      | No DevDeployJob       | Deploy on the development server.|
| Staging        | https://pw-qbg-stage.penpencil.co/question-answer/  | https://jenkins.penpencil.co/job/Staging/job/pw-qbg/   | Merge code into the staging branch and then start build at jenkins for deployment, then check to ensure everything is working correctly                           |
| Production     | https://www.pw.live/question-answer        | https://jenkins.penpencil.co/job/Production/job/pw-qbg/| Merge code into the main branch for automatic deployment, then check to ensure everything is working correctly.                   |
## Contributors <a name="contributors"></a>
​
A big thanks to our contributors for their valuable input and effort:
​
- Vicky Paul - (@vicky_paul)
- Amandeep Singh - (@amandeep.singh75)
- Aditya Kumari - (@adityaKumari31)
- Rupali Bharti - (@rupali1033122)

​
Thank you for your support and contributions!
​
## License <a name="license"></a>
​
This project is licensed under **PW**.
